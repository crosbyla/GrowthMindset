# Book Summary: _Make it Stick_ by Peter Brown

Two psychologists and (thankfully) one writer present the latest research on learning and, in so doing, refutes some of our most popular learning techniques (such as 'practice, practice, practice' and my favorite 'read and reread'). At the end of the book, the following eight concrete techniques are offered:

1. *Retrieving* - practice retrieving new (and old) learning (self-quizzing).
2. *Spacing* - space out your retrieval practice, leave time to forget in between practice sessions.
3. *Interleaving* - alternate working on different problems facilitates spacing and forgetting (making learning more difficult, which improves learning).
4. *Elaboration* - try to find additional layers of meaning in the new material.
5. Generation - attempt to answer a question or solve a problem before looking at the answer (experiential learning).
6. *Reflection* - a combination of retrieval practice and elaboration that adds layers to learning new material. Ask your self questions.
7. *Calibration* - to avoid various cognitive illusions, use an objective instrument to adjust your sense of what you know and don't know.
8. *Mnemonic* devices - build memory palaces to help yourself retrieve what you have learned.

The first four items on the list are the most useful, so you should put most of your effort into implementing those steps.
The main goal is to "practice the way you play."
