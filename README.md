# All about growth

This directory is dedicated to aggregating advice, insights, and ideas all based upon the premise of growth.
Dr. Carol Dweck, a psychologist at Stanford, has done extensive research about how mindsets influence performance and outcomes.
Here, we have a few goals:

- [ ] Dispel the myth that one's intelligence is a fixed quantity.
- [ ] Document ways that people have overcome adversity to succeed.
- [ ] Draft a publication or directive to distribute to incoming students.
- [ ] Learn and document best practices concerning cognition and learning.
- [ ] Train students from day one in what tools and resources are available to them.
- [ ] Establish a network of like-minded peers and advocates.
