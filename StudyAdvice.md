# Key strategies to increase retention

1. Frequent, regular testing---either self-testing or by instructor
2. Spaced repetition (minimum 24 hours to allow recovery)
3. Interleaved practice
4. Elaboration: connecting the material/lesson to something else that you've learned elsewhere, especially real-life experiences
5. Generation: attempt a solution to problem even before you have been taught how to solve it by the instructor

## Actionable advice from _Make it Stick_

- Always do the reading prior to lecture
- Anticipate test questions and their answers as you read
- Answer rhetorical questions in your head during lectures to test your retention of the reading
- Review study guides, find terms you can't recall or don't know, and relearn those terms
- Copy bolded terms and their definitions into a reading notebook, making sure that you understand them
- Take the practice test that is provided online by your professor; from there you discover which concepts you don't know and make a point to learn them
- Reorganize the course information into a study guide of your design
- Write out concepts that are detailed or important, post them above your bed, and test yourself on them from time to time
- Use a cumulative deck of flashcards---don't remove items from the deck until the end of the course.
- Space out your review and practice over the duration of the course

# More details:

## Do the reading before class

This is extremely important. First, the lecture makes more sense when you’ve read the assignment. Second, you won’t end up asking no questions, or dumb questions, but you will ask pertinent questions – this will not only aid your comprehension, but your prof will tag you as a sharp student and this has a positive effect on how you’re evaluated (even if the prof doesn’t know it).

I treated college like a job, and had set reading/study times during the day for all my classes, which I would spend in the library. This kept me from procrastinating, and provided me with free time in the evenings and on weekends to relax.

## Take high-level notes while you read – do not use a highlighter, do not outline, do not take detailed notes (yet).

Highlighting is ineffective. Don’t bother with it.

Instead, keep a notebook – either on your laptop or (my preference) in a paper notebook. In this notebook, write summary statements of what you read as you go, leaving gaps which you will fill in during the lecture and at other times. You don’t want to outline the text; you want a skeleton frame of the ideas as they make sense to you, according to your style of thinking. Jot down page numbers so you can refer back to your text.

Also write marginal notes in the book. At the top of the page, write the big ideas and important details where they occur, so you can find them later.

And don’t forget to write down questions you have for the instructor!

Remember, you’re not trying to re-write the book in your notes! You’re creating a mental scaffolding and index so that the concepts cohere – rather than being a blizzard of details – and so that you can easily find relevant passages on review.

I found it useful to invent my own system of abbreviations. E.g., in my notes I stopped writing the letter “i” and replaced it with a dot. The suffix “tion” became ’n. The word Constitution became \!o. This makes your note-taking quicker, and thwarts people who want to “borrow” your notes, putting you at risk of not having them when you need them.

And here’s the trick – you don’t need to learn all the details on the first round!

## Get interested in the subject, no matter what it is.

Put out of your mind the idea that you’re studying for a test. Get into the perspective that you want to master this subject, to really understand it.

If the text is boring – and some texts are horribly boring – admit that and focus on gleaning the pertinent information from this awful brick. Treat it like a game, where some books are helpers and other books are foes.

Be bound and determined to discover what’s cool and useful and worthwhile in this subject. Your mindset is incredibly important!

I used to construct stories, and that’s how I took notes on the text. So if it was a history class, rather than string a bunch of dates together, I’d construct scenarios... BECAUSE this happened, then that happened, SO the next thing occurred, which led to....

I would do that for the sciences, too. Because of this, therefore that. Everything can be made into a story, and stories are much easier to learn and remember than facts are.

## Use class time wisely.

OK, so you’ve read your text before class, you’ve avoided outlining and highlighting, you haven’t attempted to note all the details, your book has topics and important facts noted at the tops of pages, you have a notebook with key ideas in it and lots of blank lines to fill in later, and there are questions written in the book margins and in your notebook.

As the lecture progresses, fill in the blank spaces with your lecture notes. Do this in a different color pen so you can tell lecture notes from book notes, and/or note the lecture dates in the margins.

Also turn to the relevant passages in the book as the lecture progresses. As your questions are answered, note those answers. If a question isn’t answered, ask it!

At the end of the class, two things will have happened.

1\. You will have produced a set of notes cross-referenced with the text and integrated with lecture notes. This will make studying tremendously easier!

2\. You will have reviewed your own notes and the book text, and this re-reading will make the details start etching themselves into your brain. (This is why you don’t try to absorb or note all the details when you first read – this is accomplished by re-reading, not first reading.)

## Review before reading the next assignment.

Before you read your next assignment in the text, glance through your notebook and re-establish your “place” in the “story“.

If something isn’t clear, go back to the text, look it up, and get clarity.

This is key – understanding a text does not come from reading it once and taking notes... it comes from reviewing it regularly, from re-reading and re-thinking.

Repeat this pattern: Take high-level notes, make brief marginal notes, write lecture notes in the blank spaces, review notes before reading the next assignment.

## Schedule study sessions a few days before exams.

Get together with students who are on the ball and review the material a few days before each exam, not the night before.

As you go, write notes from these sessions in the blank spaces left in your notebook. You don’t want your book notes, lecture notes, and study session notes in separate places.

If questions come up that nobody knows the answer to, ask them in class.

## The day before an exam, give a quick review, then walk away.

By the time exam day comes, you will have reviewed this material *multiple times*, and now the details will be clear, as will the “stories” that bind them together.

The day before, flip through the book, reviewing your marginal notes and big ideas. Browse through your notes, making sure you’ve got it all down.

Then go out and forget about it.

That period of “forgetting about it” is extremely important – it gives your non-conscious mind the time and space to do its work, and you will not only be more confident and better rested, but you’ll recall things much more clearly.

The big take away: Don’t try to comprehend everything at once – instead, structure your reading and note-taking habits so that you are re-reading and reviewing regularly and your brain will naturally learn the material more efficiently and with much better recall.

And if you’re a student who’s interested in optimizing your time more generally, I recommend doing an online search for “GTD for students” – you’ll find resources for implementing the “Getting Things Done” method, which I currently use for both my business and personal calendars.

## Papers and Projects

The biggest mistakes students make are to (a) not set intermediate deadlines, and (b) if they do set those deadlines, they don’t schedule time to complete them.

So for example, you have a paper due near the end of the term. Take that deadline and “back-chain” it, deciding when you’ll have your topic, your abstract, your outline, your first draft, and your final draft.

Most students who plan at all will stop there, but that’s just the first step of 3:

1\. SET DEADLINES FOR YOUR STEPS TOWARD YOUR GOAL.

2\. SCHEDULE TIME IN YOUR WEEKLY CALENDAR TO ACCOMPLISH THOSE STEPS

3\. SET “CONSEQUENCE DATES” TO MAKE SURE THE WORK GETS DONE.

If you’re treating college like a day job and getting your work done by, say, 6:00 every day in between your classes, you should always know what you’re doing in the library or lab – it should never be a general task to “study”.

Treat yourself like an entrepreneur – at your job of college, you have to run the projects and make sure they get to the clients (your instructors) complete and on time.

That doesn’t happen just by setting goals, but rather by assigning workers to the tasks, and making sure they schedule time to get those tasks done by deadline.

In this case, you’re the boss and the workers, and if you can do this in college it’s great training for eventually striking out on your own, doing what you want for a living, and running the show!

In college, the best “consequence dates” are meetings with professors.

This is like a client meeting in business. If you schedule a demo of the prototype with the client, then you’d better have that prototype ready!

For a paper, I recommend setting up a time to discuss your topic, and later setting up a meeting to review your draft.

Don’t walk in with nothing in that first meeting. Schedule time to do some research so you’re either asking if your topic is good, or you’re asking for clarification. You’ll walk out of that meeting with confidence that you’re on the right track.

Then schedule time to write an abstract (a couple of paragraphs explaining to yourself what you’re writing about), then to produce an outline based on your research, then to produce a draft based on that outline.

Submit your draft to your instructor with enough time before your meeting so s/he can review it.

Don’t expect a detailed analysis. What you’ll get are general – but important – responses to the major points, your premises and conclusions, and any obvious errors.

After this meeting, schedule time to produce the final draft.

Remember, your readings and your papers are projects – treat them as you would a project for a paying client if you ran your own one-person business.

Finding a good calendar is key to this. I went to school back in the day of the paper “day planner” but now there’s software to help you.

Trust me, once you get into the swing of this, and you find yourself getting your work done and having most evenings free to unwind, you’ll get addicted to it, and you’ll never want to go back!
